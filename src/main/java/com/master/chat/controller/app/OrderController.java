package com.master.chat.controller.app;

import com.master.chat.framework.base.BaseAppController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单接口
 *
 * @author: yang
 * @date: 2023/5/4
 * @version: 1.0.0
 * Copyright Ⓒ 2022 Master Computer Corporation Limited All rights reserved.
 */
@RestController(value = "appOrderController")
@RequestMapping("/app/order")
public class OrderController extends BaseAppController {


}
