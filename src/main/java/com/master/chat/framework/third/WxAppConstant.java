package com.master.chat.framework.third;

/**
 * 微信小程序常量
 *
 * @author: Yang
 * @date: 2023/1/9
 * @version: 1.0.0
 * Copyright Ⓒ 2022 Master Computer Corporation Limited All rights reserved.
 */
public interface WxAppConstant {

    String APP_ID = "";

    String APP_SECRET = "";

}
