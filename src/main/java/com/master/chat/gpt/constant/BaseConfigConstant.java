package com.master.chat.gpt.constant;

/**
 * 配置常量
 *
 * @author: Yang
 * @date: 2023/5/4
 * @version: 1.0.0
 * Copyright Ⓒ 2022 Master Computer Corporation Limited All rights reserved.
 */
public interface BaseConfigConstant {

    /**
     * 基础信息
     */
    String BASE_INFO = "baseInfo";

    /**
     * 应用信息
     */
    String APP_INFO = "appInfo";

    /**
     * 微信信息
     */
    String WX_INFO = "wxInfo";

    /**
     * 额外信息
     */
    String EXTRA_INFO = "extraInfo";

}
