package com.master.chat.common.constant;

/**
 * 短信常量类
 *
 * @author: Yang
 * @date: 2023/01/31
 * @version: 1.0.0
 * Copyright Ⓒ 2023 Master Computer Corporation Limited All rights reserved.
 */
public interface SmsConstant {

    /**
     * 短信签名
     */
    String SIGN_NAME = "";

    /**
     * 验证码短信模板
     */
    String AUTHCODE_TEMPLATE = "";

}
